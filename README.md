# Personal Details Web Service #

To build the project, execute the following from the project root directory: ***mvn package -Dmaven.test.skip=true***

To run the project, execute the following from the project root directory: ***java -cp target/PersonalDetailsService-1.0.jar:target/alternateLocation/\* PersonalDetailsService***  


**View javadocs here: JavaDocs/index.html **  
**View Coverage report here: Coverage Report/index.html **

**Details**  
Framework - Spark Java micro framework  
Database - H2 in-memory embedded database  
Persistence - JPA with Hibernate  

**Base URL: http://localhost:4567** 

## Methods ##

### Create a Person record ###
Path: /person  
Request Method: POST  
Response Type: Application/JSON  
Parameters: First Name, Surname, Date of Birth (yyyy/MM/dd), Sex (M or F), Email  
Successful Return: HTTP 200, ID of the person that was created.  
Failed Return: HTTP 404, {Error Message}

### Retrieve a Person Record ###
Path: /person/id  
Request Method: GET  
Response Type: Application/JSON  
Parameters: A person's ID number  
Failed Return: HTTP 404, {Error Message}


### Authentication ###

I've implemented a naive authentication procedure within an API filter. This essentially means that before any service endpoint is executed, the filter will be executed first. I created a basic token (xyz123) that a user would need to enter along with the query parameters for the POST method. The security of this method is reliant on application layer security (SSL) to ensure that the token is not revealed in the body of the request.

The main consideration in securing the GET method is the fact that data is sent in the HTTP header as opposed to the body of the POST method. This means that SSL will not be able to protect sensitive data. If the data is in fact sensitive, it would be better to implement the API as a POST. For internal services however, it is possible to limit access to a guest method by the requester's IP address. But this is more access control than authentication.

Ideally, to manage authentication, give enough time, I would implement ApiAxle (http://apiaxle.com). ApiAxle allows you to create tokens for RESTful APIs, provides rate limiting to prevent DoS and has a very sophisticated analytics engine to allow you to keep track of the calls to the API.