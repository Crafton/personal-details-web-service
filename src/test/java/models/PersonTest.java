/**
 * Created by Crafton Williams on 6/22/14.
 */


package models;

import junit.framework.TestCase;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PersonTest extends TestCase {

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    /**
     * Unit test - Create person object
     * @throws Exception
     */
    public void testPersonObject() throws Exception {

        Person p = new Person();
        p.setFirstName("Crafton");
        p.setSurName("Williams");
        p.setSex("M");

        Date date = formatter.parse("1979/12/03");

        p.setDateOfBirth(date);
        p.setEmail("crafton.williamsjr@gmail.com");

        assertNotNull(p);
        assertEquals("Crafton", p.getFirstName());
        assertEquals("Williams", p.getSurName());
        assertEquals("M", p.getSex());
        assertEquals("1979/12/03", formatter.format(p.getDateOfBirth()));

    }

    /**
     * Unit test - Test connection Manager
     * @throws Exception
     */

    public void testConnectionManager() throws Exception {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("Person");
        EntityManager theManager = factory.createEntityManager();

        assertNotNull(theManager);
    }

    /**
     * Integration test - Connect, Store and retrieve records
     * @throws Exception
     */
    public void testPersistentPersonObject() throws Exception {
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("Person");
        EntityManager theManager = factory.createEntityManager();
        theManager.getTransaction().begin();
        assertNotNull(theManager);

        Person p1 = new Person("Crafton", "Williams", formatter.parse("1979/12/03"), "M", "crafton.williamsjr@gmail.com");
        theManager.persist(p1);
        Person p2 = new Person("Bob", "Williams", formatter.parse("1949/12/03"), "M", "bob.williamsjr@gmail.com");
        theManager.persist(p2);
        Person p3 = new Person("Jim", "Johnson", formatter.parse("1938/12/03"), "M", "jim.johnson@gmail.com");
        theManager.persist(p3);
        Person p4 = new Person("Joe", "Williams", formatter.parse("1921/12/03"), "M", "joe.williamsjr@gmail.com");
        theManager.persist(p4);
        Person p5 = new Person("Sue", "Williams", formatter.parse("1989/12/03"), "F", "sue.williamsjr@gmail.com");
        theManager.persist(p5);

        theManager.getTransaction().commit();

        //retrieve user with specific ID number
        Person foundP = theManager.find(Person.class, 3);
        assertNotNull(foundP);
        assertEquals("Jim", foundP.getFirstName());
        assertEquals("Johnson", foundP.getSurName());
        assertEquals("M", foundP.getSex());
        assertEquals("jim.johnson@gmail.com", foundP.getEmail());
        assertEquals("1938/12/03", formatter.format(foundP.getDateOfBirth()));

        Person notFoundP = theManager.find(Person.class, 10);
        assertNull(notFoundP);

    }
}