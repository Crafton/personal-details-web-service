/**
 * Created by Crafton Williams on 6/22/14.
 */


package models;


import javax.persistence.*;
import java.util.Date;

@Entity
public class Person {
    /**
     * The Person id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer personId;

    /**
     * The First name.
     */
    @Column(nullable = false)
    private String firstName;
    /**
     * The Sur name.
     */
    private String surName;
    /**
     * The Date of birth.
     */
    @Column(nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

    /**
     * The Sex.
     */
    @Column(length=1)
    private String sex;

    /**
     * The Email.
     */
    private String email;

    /**
     * Instantiates a new Person.
     */
    public Person() {
    }

    /**
     * Instantiates a new Person.
     *
     * @param firstName the first name
     * @param surName the sur name
     * @param dateOfBirth the date of birth
     * @param sex the sex
     * @param email the email
     */
    public Person(String firstName, String surName, Date dateOfBirth, String sex, String email) {
        this.firstName = firstName;
        this.surName = surName;
        this.dateOfBirth = dateOfBirth;
        this.sex = sex;
        this.email = email;
    }

    /**
     * Gets person id.
     *
     * @return the person id
     */
    public Integer getPersonId() {
        return personId;
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets sur name.
     *
     * @return the sur name
     */
    public String getSurName() {
        return surName;
    }

    /**
     * Sets sur name.
     *
     * @param surName the sur name
     */
    public void setSurName(String surName) {
        this.surName = surName;
    }

    /**
     * Gets date of birth.
     *
     * @return the date of birth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets date of birth.
     *
     * @param dateOfBirth the date of birth
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * Gets sex.
     *
     * @return the sex
     */
    public String getSex() {
        return sex;
    }

    /**
     * Sets sex.
     *
     * @param sex the sex
     */
    public void setSex(String sex) {
        this.sex = sex;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }
}
