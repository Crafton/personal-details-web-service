/**
 * Created by Crafton Williams on 6/22/14.
 */

import Utils.JsonTransformer;
import models.Person;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Optional;
import java.util.function.Function;

import static spark.Spark.*;
import static spark.Spark.halt;

public class PersonalDetailsService {

    /**
     * The constant ERROR.
     */
    private static final Integer ERROR = 404;
    /**
     * The constant OK.
     */
    private static final Integer OK = 200;


    /**
     * Main void.
     *
     * @param args the args
     */
    public static void main(String[] args){

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        Function<String, String> errorMessage =
                (message) -> "{Code: Error, Message: " + message + "}";
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("Person");
        EntityManager theManager = factory.createEntityManager();

        setPort(80);

        /**
         * Authenticate POST requests
         */
        before((request, response) -> {
            if(request.requestMethod().contentEquals("POST")) {
                Optional<String> optionalToken = Optional.ofNullable(request.queryParams("token"));
                if (optionalToken.isPresent()) {
                    if (!optionalToken.get().contentEquals("xyz123")) {
                        halt("Invalid Token.");
                    }
                } else {
                    halt("This is a protected service. Please enter your token to proceed. Hint: Your token is xyz123");
                }
            }
        });

        /**
         * Get a person record
         *
         * @param personId the person_id
         */
        get("/person/:personId", "application/json", (request, response) -> {
            response.type("application/json");
            Optional<String> optionalId = Optional.ofNullable(request.params("personId"));
            Optional<Person> optionalPersonFound = Optional.empty();
            if(optionalId.isPresent()){
                Integer id = Integer.parseInt(optionalId.get());
                optionalPersonFound = Optional.ofNullable(theManager.find(Person.class, id));
                if(optionalPersonFound.isPresent()){
                    response.status(OK);
                    return optionalPersonFound.get();
                }else{
                    response.status(ERROR);
                    return errorMessage.apply("A user with that ID has not been found. Please try again");
                }
            }else{
                response.status(ERROR);
                return  errorMessage.apply("You must enter an ID to proceed");
            }
        }, new JsonTransformer());


        /**
         * Creates a new person record in the database
         *
         * @param firstname the first name. This is a required field
         * @param surname the sur name.
         * @param dateofbirth the date of birth. This is a required field.
         * @param sex the sex. This can only be M or F.
         * @param email the user's e-mail address.
         */
        post("/person", "application/json", (request, response) -> {
            response.type("application/json");
            Person person = new Person();
            Optional<String> optionalFirstName = Optional.ofNullable(request.queryParams("firstname"));
            Optional<String> optionalSurName = Optional.ofNullable(request.queryParams("surname"));
            Optional<String> optionalDateOfBirth = Optional.ofNullable(request.queryParams("dateofbirth"));
            Optional<String> optionalSex = Optional.ofNullable(request.queryParams("sex"));
            Optional<String> optionalEmail = Optional.ofNullable(request.queryParams("email"));

            if(!optionalFirstName.isPresent() || !optionalDateOfBirth.isPresent()){
                errorMessage.apply("Please enter at least a First Name and a Date of Birth");
            }else{
                if(optionalFirstName.isPresent()){
                    person.setFirstName(optionalFirstName.get());
                }
                if(optionalSurName.isPresent()){
                    person.setSurName(optionalSurName.get());
                }
                if(optionalDateOfBirth.isPresent()){
                    try {
                        person.setDateOfBirth(formatter.parse(optionalDateOfBirth.get()));
                    } catch (ParseException e) {
                       return errorMessage.apply("There is a problem with your date field. Please ensure that you are using the format: yyyy/MM/dd");
                    }
                }
                if(optionalSex.isPresent()){
                    if(optionalSex.get().contentEquals("M") || optionalSex.get().contentEquals("F")){
                        person.setSex(optionalSex.get());
                    }else{
                        return  errorMessage.apply("Please enter either M or F for the Sex field. Letter case is important.");
                    }

                }
                if(optionalEmail.isPresent()){
                    person.setEmail(optionalEmail.get());
                }
            }
            try {
                theManager.getTransaction().begin();
                theManager.persist(person);
                theManager.getTransaction().commit();
            }catch (Exception ex){
                theManager.getTransaction().rollback();
            }

            return person.getPersonId();
        }, new JsonTransformer());
    }

}
