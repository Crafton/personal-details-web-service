package Utils;

import com.google.gson.Gson;
import spark.ResponseTransformer;

/**
 * Created by Crafton Williams on 6/22/14.
 */
public class JsonTransformer implements ResponseTransformer{
    /**
     * Create a new Gson object.
     */
    private Gson gson = new Gson();

    @Override
    public String render(Object model) {
        return gson.toJson(model);
    }
}
